'use strict';
var net = require('net');
var connect = require('connect');
var http = require('http');
var debug = require('debug')('denon_proxy');

var run = function (listenPort, host, port) {
    var app = connect();
    var sendCommand = function (command) {
        var client = net.connect(
            {host: host, port: port},
            function () {
                debug('command "%s"', command);
                client.write(command + '\r');
                client.end();
            }
        );
    };
    app.use(function (req, res) {
        var command = req.url.replace(/^\//, "");
        sendCommand(command);
        res.end(command);
    });

    //create node.js http server and listen on port
    http.createServer(app).listen(listenPort);
    debug('port %d relayed to %s:%d.', listenPort, host, port);
};
module.exports.run = run;
